# Code Examples

BankCardController.php - example api controller. This controller is responsible for working with bank cards

BinotelIpMiddleware.php - Simple protector. This defender is responsible for protecting the callback-link from third-party requests.

FacebookApiConversion.php - Sending server events to the facebook server using the official library for PHP. Used by the marketing department when tracking purchases from the site

LotService.php - Service of work with the remains of goods. The class applies both directly to the work of crm, and for updating the remains from 1C

ProductRequest.php - Request to create a product

Sms.php - Service for sending SMS and receiving account balance using the official library

SmsController.php - Сontroller example that works for sending sms using the official library
