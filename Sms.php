<?php

namespace App\Services\External;

use App\Models\SmsLog;
use Daaner\TurboSMS\Facades\TurboSMS;

final class Sms
{
    private TurboSMS $sms_service;

    public function __construct(TurboSMS $sms_service, $sms_sender = null)
    {
        $this->sms_service = $sms_service;
        $this->sms_service::setApi(get_option('turbo_sms_api_key', config('turbosms.api_key')));
        $this->sms_service::setSMSSender($sms_sender ?? (get_option('turbo_sms_sender', config('turbosms.sms_sender'))));
    }

    public function getBalance(): float
    {
        return $this->sms_service::getBalance() ?? 0;
    }

    public function sendSms(string $phone, string $text, int $order_id): array
    {
        $statusSendSms = $this->sms_service::sendMessages($phone, $text);

        if ($statusSendSms['success']){
            SmsLog::create([
                "order_id" => $order_id,
                "sms_id" => $statusSendSms['result']['0']['message_id'],
                "phone" => $phone,
                "message" => $text,
                'balance' => $this->sms_service::getBalance() ?? 0,
            ]);
        }

        return $statusSendSms;
    }
}
