<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController as Controller;
use App\Http\Requests\Bank\BankCardRequest;
use App\Http\Requests\Bank\BankCardUpdateRequest;
use App\Models\BankCard;
use App\Repositories\Domain\BankRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BankCardController extends Controller
{
    public function index(Request $request, BankRepository $bankRepository): JsonResponse
    {
        try {
            $cards = $bankRepository->getCards($request);
            return $this->returnSuccess('Successfully loaded cards!', $cards);
        } catch (\Throwable $exception) {
            \Log::error($exception->getMessage());
            return $this->returnError($exception->getMessage());
        }
    }

    public function show(int $id): JsonResponse
    {
        try {
            $card = BankCard::firstOrFail($id);
            return $this->returnSuccess('Successfully loaded card!', $card);
        } catch (\Throwable $exception) {
            \Log::error($exception->getMessage());
            return $this->returnError($exception->getMessage());
        }

    }

    public function store(BankCardRequest $request): JsonResponse
    {
        try {
            $customer = BankCard::create($request->validated());
            return $this->returnSuccess('Successfully created card!', $customer, 201);
        } catch (\Throwable $exception) {
            \Log::error($exception->getMessage());
            return $this->returnError($exception->getMessage());
        }
    }

    public function update(BankCardUpdateRequest $request, BankCard $card): JsonResponse
    {
        try {
            $card->fill($request->validated());
            $card->save();
            return $this->returnSuccess('Successfully created card!', $card);
        } catch (\Throwable $exception) {
            return $this->returnError($exception->getMessage());
        }
    }

    public function destroy(BankCard $card): JsonResponse
    {
        try {
            $card->delete();
            return $this->returnSuccess('Successfully deleted card!', null, 302);
        } catch (\Throwable $exception) {
            return $this->returnError($exception->getMessage());
        }
    }
}
