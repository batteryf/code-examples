<?php

namespace App\Http\Requests\Product;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Department;
use App\Models\ProductManufacturer;
use App\Models\ProductMaterial;
use App\Models\ProductModel;
use App\Models\Site;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

final class ProductRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'status' => [Rule::in(array_keys(Product::STATUSES))],
            'title' => ['required','string', 'max:255'],
            'full_title' => ['nullable', 'string', 'max:255'],
            'description' => ['nullable','string', 'max:65000'],
            'sku' => ['nullable','string', 'max:255'],
            'isbn' => ['nullable','string', 'max:255'],
            'sale_price' => ['required','regex:/^\d+(\.\d{1,5})?$/'],
            'discount_price' => ['nullable','regex:/^\d+(\.\d{1,5})?$/'],
            'upsell_manager_motivation' => ['nullable','regex:/^\d+(\.\d{1,5})?$/'],
            'images' => ['required','array'],
            'images.*.image' => ['required','string'],
            'category_id' => ['required', Rule::in(array_keys(ProductCategory::getForSelect()))],
            'model_id' => ['nullable', Rule::in(array_keys(ProductModel::getForSelect()))],
            'material_id' => ['nullable', Rule::in(array_keys(ProductMaterial::getForSelect()))],
            'manufacturer_id' => ['nullable', Rule::in(array_keys(ProductManufacturer::getForSelect()))],
            'department_id' => ['nullable', Rule::in(array_keys(Department::getForSelect()))],
            'sort' => ['integer'],
            'sites' => ['nullable', 'array'],
            'sites.*' => ['integer', Rule::in(array_keys(Site::getForSelect()))],
            'minimum' => ['integer'],
            'default_width' => ['nullable', 'numeric'],
            'default_height' => ['nullable', 'numeric'],
            'default_length' => ['nullable', 'numeric'],
            'default_weight' => ['nullable', 'numeric'],
        ];
    }
}
