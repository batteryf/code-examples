<?php


namespace App\Repositories;


use App\Models\Order;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\ServerSide\Content;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;
use Illuminate\Support\Arr;

final class FacebookApiConversionRepository
{
    /**
     * @var string|array|\ArrayAccess|mixed
     */
    private string $pixelId;
    /**
     * @var string|array|\ArrayAccess|mixed
     */
    private string $accessToken;
    /**
     * @var string|array|\ArrayAccess|mixed
     */
    private string $testCode;
    /**
     * @var \DateTime
     */
    private \DateTime $date;

    /**
     * FacebookApiConversionRepository constructor.
     */
    public function __construct()
    {
        try {
            $settingApiConversion = getSettings('facebook_api_conversion', true);
            $this->pixelId = Arr::has($settingApiConversion, 'pixel_id') ? Arr::get($settingApiConversion, 'pixel_id') : '';
            $this->accessToken = Arr::has($settingApiConversion, 'access_token') ? Arr::get($settingApiConversion, 'access_token') : '';
            $this->testCode = Arr::has($settingApiConversion, 'test_code') ? Arr::get($settingApiConversion, 'test_code') : '';
            $this->date = new \DateTime();
            unset($settingApiConversion);
            if (empty($this->pixelId) || empty($this->accessToken)) {
                \Log::critical("Задайте Facebook Conversion Pixel_id или Facebook Conversion access_token в настройках");
                return false;
            } else {
                return true;
            }
        } catch (\Throwable $exception) {
            \Log::critical($exception->getMessage());
            return false;
        }
    }

    /**
     * @param $orderId
     * @param $productsData
     * @param $fbp
     * @param $fbc
     * @param $facebookEventId
     * @param $userIP
     * @param $userAgent
     * @param $baseUrl
     * @return bool
     */
    public function purchase($orderId, $productsData, $fbp, $fbc, $facebookEventId, $userIP, $userAgent, $baseUrl): bool
    {
        if ($orderId) {
            $order = Order::where('id', '=', $orderId)->firstOrFail();
        } else {
            return false;
        }

        $this->init();

        $events = [];

        $user_data_0 = (new UserData())
            ->setClientIpAddress($userIP)
            ->setClientUserAgent($userAgent);
        if (isset($fbp) && !empty($fbp)) {
            $user_data_0->setFbp($fbp);
        }
        if (isset($fbc) && !empty($fbc)) {
            $user_data_0->setFbc($fbc);
        }
        $products = [];
        foreach ($productsData as $product) {
            $products[] = new Content(["product_id" => $product['product_id'], "quantity" => $product['qty']]);
        }

        $custom_data_0 = (new CustomData())
            ->setValue(floatval($order->order_total))
            ->setCurrency("UAH")
            ->setContents($products)
            ->setContentType("product")
            ->setOrderId($orderId);

        $event_0 = (new Event())
            ->setEventName("Purchase")
            ->setEventTime($this->date->getTimestamp())
            ->setUserData($user_data_0)
            ->setCustomData($custom_data_0)
            ->setEventSourceUrl($baseUrl)
            ->setEventId($facebookEventId)
            ->setActionSource("website");

        array_push($events, $event_0);

        $request = (new EventRequest($this->pixelId))
            ->setEvents($events);

        if (!empty($this->testCode)) {
            $request = $request->setTestEventCode($this->testCode);
        }

        $request->execute();
        return true;
    }

    /**
     * @param $productsData
     * @param $fbp
     * @param $fbc
     * @param $facebookEventId
     * @param $userIP
     * @param $userAgent
     * @param $baseUrl
     * @return bool
     */
    public function viewContent($productsData, $fbp, $fbc, $facebookEventId, $userIP, $userAgent, $baseUrl): bool
    {
        $this->init();

        $events = [];

        $user_data_0 = (new UserData())
            ->setClientIpAddress($userIP)
            ->setClientUserAgent($userAgent);

        if (isset($fbp) && !empty($fbp)) {
            $user_data_0->setFbp($fbp);
        }
        if (isset($fbc) && !empty($fbc)) {
            $user_data_0->setFbc($fbc);
        }

        $custom_data_0 = (new CustomData())
            ->setValue((string)$productsData['price'])
            ->setCurrency('UAH')
            ->setContentName($productsData['name'])
            ->setContentCategory($productsData['category'])
            ->setContentIds([$productsData['id']])
            ->setContentType("product");

        $event_0 = (new Event())
            ->setEventName("ViewContent")
            ->setEventTime($this->date->getTimestamp())
            ->setUserData($user_data_0)
            ->setCustomData($custom_data_0)
            ->setEventSourceUrl($baseUrl)
            ->setActionSource("website")
            ->setEventId($facebookEventId);
        array_push($events, $event_0);

        $request = (new EventRequest($this->pixelId))
            ->setEvents($events);

        if (!empty($this->testCode)) {
            $request = $request->setTestEventCode($this->testCode);
        }

        $request->execute();
        return true;
    }

    /**
     * @param $productData
     * @param $fbp
     * @param $fbc
     * @param $facebookEventId
     * @param $userIP
     * @param $userAgent
     * @param $baseUrl
     * @return bool
     */
    public function addToCart($productData, $fbp, $fbc, $facebookEventId, $userIP, $userAgent, $baseUrl): bool
    {
        $this->init();

        $events = [];

        $user_data_0 = (new UserData())
            ->setClientIpAddress($userIP)
            ->setClientUserAgent($userAgent);

        if (isset($fbp) && !empty($fbp)) {
            $user_data_0->setFbp($fbp);
        }
        if (isset($fbc) && !empty($fbc)) {
            $user_data_0->setFbc($fbc);
        }


        $products = [];
        foreach ($productData as $product) {
            $products[] = new Content(["product_id" => $product['id'], "quantity" => $product['quantity'], "item_price" => $product['price']]);
        }

        $firstProduct = Arr::first($productData);

        $custom_data_0 = (new CustomData())
            ->setValue((string)$firstProduct['price'])
            ->setCurrency('UAH')
            ->setContentName($firstProduct['name'])
            ->setContentCategory($firstProduct['category'])
            ->setContentIds(Arr::pluck($productData, 'id'))
            ->setContents($products)
            ->setContentType("product");

        $event_0 = (new Event())
            ->setEventName("AddToCart")
            ->setEventTime($this->date->getTimestamp())
            ->setUserData($user_data_0)
            ->setCustomData($custom_data_0)
            ->setEventSourceUrl($baseUrl)
            ->setActionSource("website")
            ->setEventId($facebookEventId);
        array_push($events, $event_0);

        $request = (new EventRequest($this->pixelId))
            ->setEvents($events);

        if (!empty($this->testCode)) {
            $request = $request->setTestEventCode($this->testCode);
        }

        $request->execute();
        return true;
    }

    /**
     * @param $productData
     * @param $fbp
     * @param $fbc
     * @param $facebookEventId
     * @param $userIP
     * @param $userAgent
     * @param $baseUrl
     * @return bool
     */
    public function initiateCheckOut($productData, $fbp, $fbc, $facebookEventId, $userIP, $userAgent, $baseUrl): bool
    {
        $this->init();

        $events = [];

        $user_data_0 = (new UserData())
            ->setClientIpAddress($userIP)
            ->setClientUserAgent($userAgent);

        if (isset($fbp) && !empty($fbp)) {
            $user_data_0->setFbp($fbp);
        }
        if (isset($fbc) && !empty($fbc)) {
            $user_data_0->setFbc($fbc);
        }
        $custom_data_0 = (new CustomData())
            ->setValue($productData['totalCost'])
            ->setCurrency("UAH")
            ->setContentType("product")
            ->setContentIds($productData['ids'])
            ->setNumItems($productData['count']);

        $event_0 = (new Event())
            ->setEventName("InitiateCheckout")
            ->setEventTime($this->date->getTimestamp())
            ->setUserData($user_data_0)
            ->setCustomData($custom_data_0)
            ->setEventSourceUrl($baseUrl)
            ->setActionSource("website")
            ->setEventId($facebookEventId);

        array_push($events, $event_0);

        $request = (new EventRequest($this->pixelId))
            ->setEvents($events);

        if (!empty($this->testCode)) {
            $request = $request->setTestEventCode($this->testCode);
        }
        $request->execute();
        return true;
    }

    /**
     * Initialize Api Conversion
     */
    private final function init()
    {
        // Initialize
        Api::init(null, null, $this->accessToken);
        $api = Api::instance();
        $api->setLogger(new CurlLogger());
    }

}
