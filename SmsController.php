<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\Sms\ExternalSmsStatusRequest;
use App\Http\Requests\Sms\SmsMessageRequest;
use App\Http\Controllers\API\ApiController as Controller;
use App\Repositories\External\SmsRepository;
use App\Services\External\Sms;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


final class SmsController extends Controller
{
    public function getBalance(Sms $sms_service): JsonResponse
    {
        try {
            $balance = $sms_service->getBalance();
            return $this->returnSuccess('Successfully loaded balance!', [
                'balance' => $balance
            ]);
        } catch (\Throwable $e) {
            \Log::error($exception->getMessage());
            return $this->returnError('Error on checking balance: '.$e->getMessage());
        }
    }

    public function send(SmsMessageRequest $request, Sms $sms_service): JsonResponse
    {
        try {
            $data = $request->validated();
            $smsStatus = $sms_service->sendSms($data['phone'], $data['text'], $data['id']);
            if ($smsStatus['success']) {
                return $this->returnSuccess($smsStatus['info'], $smsStatus);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => $smsStatus['info'],
                    'models' => $smsStatus,
                ], 500);

            }
        } catch (\Throwable $e) {
            \Log::error($exception->getMessage());
            return $this->returnError('Error on sending sms: ' . $e->getMessage());
        }
    }

    public function statusSms(ExternalSmsStatusRequest $request, SmsRepository $smsRepository)
    {
        try {
            return $this->returnSuccess(
                'Successfully update sms',
                $smsRepository::updateListSms($request)
            );
        } catch (\Throwable $e) {
            \Log::error($exception->getMessage());
            return $this->returnError('Error on get sms status: ' . $e->getMessage());
        }
    }

    public function index(SmsRepository $smsRepository, Request $request): JsonResponse
    {
        try {
            $listSms = $smsRepository::getListSms($request);
            return $this->returnSuccess(
                'Successfully loaded sms',
                $listSms
            );
        } catch (\Throwable $e) {
            \Log::error($exception->getMessage());
            return $this->returnError('Error on loading sms: ' . $e->getMessage());
        }
    }

}
