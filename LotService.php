<?php

namespace App\Services\Domain;

use App\Http\Requests\Lot\Lot1CRequest;
use App\Http\Requests\Lot\LotRequest;
use App\Jobs\BulkRenewalResidues;
use App\Jobs\SendLotsOnSite;
use App\Models\Lot;
use App\Models\LotProduct;
use App\Models\Site;
use App\Repositories\Domain\CurrencyValueRepository;
use App\Repositories\Domain\LotRepository;

final class LotService extends BaseDomainService
{
    public function createLot(LotRequest $request): Lot
    {

        $data = $request->validated();
        $actual_currency = CurrencyValueRepository::getActualValue();
        try {
            \DB::beginTransaction();
            $lot = Lot::create([
                'provisioner_id' => $data['provisioner_id'],
                'warehouse_id' => $data['warehouse_id'],
            ]);
            $lot_products_data = [];
            foreach ($data['lot_products'] as $product_index => $product) {
                $lot_products_data[$product_index]['lot_id'] = $lot->id;
                $lot_products_data[$product_index]['product_id'] = $product['product_id'];
                $lot_products_data[$product_index]['buy_price'] = (float)$product['buy_price_usd'] * $actual_currency;
                $lot_products_data[$product_index]['buy_price_usd'] = $product['buy_price_usd'];
                $lot_products_data[$product_index]['buy_quantity'] = $product['buy_quantity'];
                $lot_products_data[$product_index]['less_quantity'] = $product['buy_quantity'];
            }
            try {
                $lot->lot_products()->createMany($lot_products_data);
                \DB::commit();
            } catch (\Throwable $e) {
                \DB::rollback();
                throw $e;
            }
        } catch (\Throwable $e) {
            \DB::rollback();
            throw $e;
        }

        return LotRepository::getSingleLotWithAllRelations($lot->id);
    }

    public function updateLot(LotRequest $request, Lot $lot)
    {
        $data = $request->validated();
        $actual_currency = CurrencyValueRepository::getActualValue();
        $lot_data = $data;
        unset($lot_data['lot_products']);
        try {
            \DB::beginTransaction();
            $lot->fill($lot_data);
            $lot->save();
            $lot_products = [];
            try {
                foreach ($data['lot_products'] as $product) {
                    $lot_product_isset = LotProduct::where(['lot_id' => $lot->id, 'product_id' => $product['product_id']])->first();
                    if ($lot_product_isset) {
                        $lot_product_isset->buy_quantity = $product['buy_quantity'];
                        $lot_product_isset->less_quantity = $product['less_quantity'] ?? $product['buy_quantity'];
                        $lot_products[] = $lot_product_isset;
                    } else {
                        $lot_products[] = LotProduct::create([
                            'lot_id' => $lot->id,
                            'product_id' => $product['product_id'],
                            'buy_price_usd' => $product['buy_price_usd'],
                            'buy_price' => (float)$product['buy_price_usd'] * $actual_currency,
                            'buy_quantity' => $product['buy_quantity'],
                            'less_quantity' => $product['buy_quantity']
                        ]);
                    }
                }
                $lot->lot_products()->saveMany($lot_products);
                \DB::commit();
            } catch (\Throwable $e) {
                \DB::rollBack();
                throw $e;
            }
        } catch (\Throwable $e) {
            \DB::rollBack();
            throw $e;
        }
        return LotRepository::getSingleLotWithAllRelations($lot->id);
    }


    public function setLotFrom1C(Lot1CRequest $request): ?bool
    {
        $data = $request->validated();
        $actual_currency = CurrencyValueRepository::getActualValue();
        try {

            \DB::beginTransaction();

            $lot = Lot::firstOrCreate([
                'provisioner_id' => $data['provisioner_id'],
                'warehouse_id' => $data['warehouse_id'],
            ], [
                'provisioner_id' => $data['provisioner_id'],
                'warehouse_id' => $data['warehouse_id'],
            ]);
            $productsOnSite = [];
            foreach ($data['products'] as $product) {
                $lotProduct = LotProduct::updateOrCreate([
                    'lot_id' => $lot->id,
                    'product_id' => $product['product_id'],
                ], [
                    'lot_id' => $lot->id,
                    'product_id' => $product['product_id'],
                    'less_quantity' => $product['less_quantity'],
                    'buy_price' => (float)$product['buy_price'],
                    'buy_price_usd' => (float)($product['buy_price'] / $actual_currency),
                    'buy_quantity' => 0,
                    'reserved_quantity' => 0,
                ]);

                if ($lotProduct) {
                    $productsOnSite [] = [
                        'id' => $product['product_id'],
                        'status' => ($product['less_quantity'] > 0) ? 1 : 0,
                    ];
                }
            }

            if (!empty($productsOnSite) && (count($sites = Site::whereNotNull(['callback_url', 'quantity_callback_url'])->get()))) {
                foreach ($sites as $site) {
                    SendLotsOnSite::dispatch((string)$site->callback_url, (string)$site->domain, (string)$site->api_key, $productsOnSite, (string)$site->quantity_callback_url);
                }
            }

            \DB::commit();
            return true;

        } catch (\Throwable $e) {
            \DB::rollback();
            throw $e;
        }
    }

    public function setBulkLotFrom1C(Lot1CRequest $request): ?bool
    {
        try {
            $data = $request->validated();
            if (count($data['products']) > 1000) {
                $products = array_chunk($data['products'], 1);
                foreach ($products as $product) {
                    BulkRenewalResidues::dispatch($data['provisioner_id'], $data['warehouse_id'], $product)->afterCommit();
                }
            } else {
                $products = $data['products'];
                BulkRenewalResidues::dispatch($data['provisioner_id'], $data['warehouse_id'], $products)->afterCommit();
            }
            return true;
        } catch (\Throwable $e) {
            throw $e;
        }
    }
}
